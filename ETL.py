from meteostat import Point, Daily
import python_weather
import airportsdata

import pandas as pd
import geopandas
import numpy as np

from pyunpack import Archive
import glob
import pickle
import zipfile

import json
from datetime import datetime

from Functions import reduce_memory_usage, merge_csv_files_into_dataframe


def get_observed_weather_data(airports: pd.DataFrame, date_from: str, date_to: str):
    """
    Function fetches historical weather data for given airports
    https://dev.meteostat.net/python/#example

    :param airports: dataframe with airports
    :param date_from: starting fetch date in format '%Y-%m-%d'
    :param date_to: last fetch date in format '%Y-%m-%d'
    :return:
    """
    date_from = datetime.strptime(date_from, '%Y-%m-%d')
    date_to = datetime.strptime(date_to, '%Y-%m-%d')
    airports_weather_data = pd.DataFrame(columns=['iata', 'time', 'tavg', 'tmin', 'tmax', 'prcp',
                                                  'snow', 'wdir', 'wspd', 'wpgt', 'pres', 'tsun'])
    for _, airport in airports.iterrows():
        airport_location = Point(airport.lat, airport.lon)
        airport_weather_data = Daily(airport_location, date_from, date_to).fetch().reset_index()
        airport_weather_data['iata'] = airport.iata

        airports_weather_data = pd.concat([airports_weather_data, airport_weather_data], sort=False, ignore_index=True)

    return airports_weather_data
    # airports_weather_data.to_pickle("../AeroflotForecast/data/external/airports_weather_data.pkl")


def get_airports_small_data():
    source_url = ("https://www.naturalearthdata.com/" \
                  "http//www.naturalearthdata.com/" \
                  "download/10m/cultural/ne_10m_airports.zip"
                  )
    airports_data = geopandas.read_file(source_url)
    airports_data.to_pickle('../AeroflotForecast/data/external/airports_data_naturalearthdata.pkl')


def get_airports_big_data():
    airports = airportsdata.load('IATA')
    airports_data = pd.DataFrame.from_dict(airports, orient='index').reset_index(drop=True)
    with open("../AeroflotForecast/data/external/airports_data_airportsdata.json", "w") as outfile:
        json.dump(airports, outfile)


async def get_weather_online(city_name):
    """
    Fetch weather data by city name online. Current weather and few days ahead.
    :param city_name:
    :return:
    """
    async with python_weather.Client() as client:
        weather = await client.get(city_name)
        curr_temperature = weather.current.temperature
        forecast_temperature = []

        for forecast in weather.forecasts:
            forecast_temperature.append(forecast)
        return [curr_temperature, forecast_temperature]


def unpack_given_data(rar_files, folder_unpack_into):
    # we are provided with the data in the rar format
    for file in glob.glob(rar_files):
        Archive(file).extractall(folder_unpack_into)


def transform_provided_data():
    unpack_given_data("../AeroflotForecast/data/raw/datasets_rar/*.rar",
                      "../AeroflotForecast/data/raw/datasets_unrared")

    class_datasets = glob.glob("../AeroflotForecast/data/raw/datasets_unrared/CLASS*.csv")
    cabin_datasets = glob.glob("../AeroflotForecast/data/raw/datasets_unrared/CABIN*.csv")
    rasp_dataset = glob.glob("../AeroflotForecast/data/raw/datasets_unrared/RASP*.csv")

    # let's merge dataframes while reducing their size
    class_history = reduce_memory_usage(merge_csv_files_into_dataframe(class_datasets))
    cabin_history = reduce_memory_usage(merge_csv_files_into_dataframe(cabin_datasets))
    rasp_history = reduce_memory_usage(merge_csv_files_into_dataframe(rasp_dataset))

    cabin_history.to_pickle("../AeroflotForecast/data/processed/cabin_history")
    rasp_history.to_pickle("../AeroflotForecast/data/processed/rasp_history")
    class_history.to_pickle("../AeroflotForecast/data/processed/class_history")
