from IPython.display import clear_output

from hyperopt import hp, fmin, tpe, STATUS_OK, STATUS_FAIL, Trials
from catboost import CatBoostRegressor

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import logging
import datetime


def get_dates_from_range_and_weekdays(start_date: str, end_date: str, weekdays: str):
    weekdays = [int(el) for el in weekdays if el != ' ']
    format = "%d.%m.%Y"
    dates_in_between = pd.date_range(datetime.datetime.strptime(start_date, format), datetime.datetime.strptime(end_date, format))
    dates_in_between_filtered = list(filter(lambda date: date.dayofweek in weekdays, dates_in_between))
    dates_in_between_filtered = [str(date)[:10] for date in dates_in_between_filtered]
    return dates_in_between_filtered


def reduce_memory_usage(df: pd.DataFrame) -> pd.DataFrame:
    """
    Reduce memory space occupied by a DataFrame
    # Reference : https://www.kaggle.com/code/arjanso/reducing-dataframe-memory-size-by-65 @ARJANGROEN
    :param df:
    :return: compressed DataFrame
    """
    start_mem = df.memory_usage().sum() / 1024 ** 2
    print('Memory usage of dataframe is {:.2f} MB'.format(start_mem))

    for col in df.columns:
        col_type = df[col].dtype.name
        if (col_type != 'datetime64[ns]') & (col_type != 'category'):
            if col_type != 'object':
                c_min = df[col].min()
                c_max = df[col].max()

                if str(col_type)[:3] == 'int':
                    if c_min > np.iinfo(np.int8).min and c_max < np.iinfo(np.int8).max:
                        df[col] = df[col].astype(np.int8)
                    elif c_min > np.iinfo(np.int16).min and c_max < np.iinfo(np.int16).max:
                        df[col] = df[col].astype(np.int16)
                    elif c_min > np.iinfo(np.int32).min and c_max < np.iinfo(np.int32).max:
                        df[col] = df[col].astype(np.int32)
                    elif c_min > np.iinfo(np.int64).min and c_max < np.iinfo(np.int64).max:
                        df[col] = df[col].astype(np.int64)

                else:
                    if c_min > np.finfo(np.float16).min and c_max < np.finfo(np.float16).max:
                        df[col] = df[col].astype(np.float16)
                    elif c_min > np.finfo(np.float32).min and c_max < np.finfo(np.float32).max:
                        df[col] = df[col].astype(np.float32)
                    else:
                        pass
            else:
                df[col] = df[col].astype('category')
    mem_usg = df.memory_usage().sum() / 1024**2
    print("Memory usage became: ", mem_usg, " MB")

    return df


def merge_csv_files_into_dataframe(datasets_names, csv_sep=';'):
    for indx, csv_file in enumerate(datasets_names):
        if indx == 0:
            dataframe = reduce_memory_usage(pd.read_csv(csv_file, sep=csv_sep))
        else:
            dataset_file = reduce_memory_usage(pd.read_csv(csv_file, sep=csv_sep))
            dataframe = pd.concat([dataframe, dataset_file], ignore_index=True)
        clear_output(wait=True)
        logging.info(f'Dataset number {indx + 1}/{len(datasets_names)}')
        logging.info('Accumulated memory usage is {:.2f} MB'.format(dataframe.memory_usage().sum() / 1024 ** 2))
    return dataframe


def show_dataframe_stat(df: pd.DataFrame):
    da_data = {'data_type': df.dtypes.values,
               'nulls_qnt': df.isna().sum(),
               'nulls_share': df.isna().sum() / len(df),
               'non_nulls_qnt': len(df) - df.isna().sum(),
               'cardinality': df.nunique()}
    da_df = pd.DataFrame(data=da_data).sort_index()
    return da_df


def residual_plot(train_labels, train_preds, test_labels=None, test_preds=None,
                  title="Residual Plot", figsize=(9, 6), xlim=[0, 20]):
    """ Residual plot to evaluate performance of our simple linear regressor """
    plt.figure(figsize=figsize)
    plt.scatter(train_preds, train_preds - train_labels, c='blue', alpha=0.1,
                marker='o', edgecolors='white', label='Training')

    if test_labels is not None:
        plt.scatter(test_preds, test_preds - test_labels, c='red', alpha=0.1,
                    marker='^', edgecolors='white', label='Test')
    plt.xlabel('Predicted values')
    plt.ylabel('Residuals')
    plt.hlines(y=0, xmin=xlim[0], xmax=xlim[1], color='black', lw=2)
    plt.xlim(xlim)
    if test_labels is not None:
        plt.legend(loc='best')
    plt.title(title)
    plt.show()
    return


class HPOptimiser(object):
    """ Class to optimiser hyper-parameters using hyperopt on a given
        set of training and validation inputs and labels """

    def __init__(self, X_train, X_val, y_train, y_val):
        self.X_train = X_train
        self.X_val = X_val
        self.y_train = y_train
        self.y_val = y_val

    def process(self, fn_name, space, trials, algo, max_evals):
        fn = getattr(self, fn_name)
        try:
            result = fmin(fn=fn, space=space, algo=algo, max_evals=max_evals, trials=trials)
        except Exception as e:
            return {'status': STATUS_FAIL,
                    'exception': str(e)}
        return result, trials

    def catboost_reg(self, para):
        reg = CatBoostRegressor(**para['reg_params'])
        return self.train_reg(reg, para)

    def train_reg(self, reg, params):
        categorical_features_indices = np.where(self.X_train.dtypes == 'category')[0]
        reg.fit(self.X_train, self.y_train,
                eval_set=[(self.X_train, self.y_train), (self.X_val, self.y_val)],
                cat_features=categorical_features_indices,
                **params['fit_params'])
        pred = reg.predict(self.X_val)
        loss = params['loss_func'](self.y_val, pred)
        return {'loss': loss, 'status': STATUS_OK}

#%%
